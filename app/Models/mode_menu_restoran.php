<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mode_menu_restoran extends Model
{
    use HasFactory;
    protected $table = 'menu_restoran';
    protected $fillable = ['judul', 'desc', 'harga'];
}
