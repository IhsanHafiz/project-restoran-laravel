var daftarMakanan = [
	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2018/11/2pc1side.png",
		nama		: "Paket Mantab",
		deskripsi	: "2-pc Chickenjoy W/ 1 Side",
		harga		: "47.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2018/11/2pc2side.png",
		nama		: "Paket Maknyus",
		deskripsi	: "2-pc Chickenjoy W/ 2 Side",
		harga		: "50.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc1side.png",
		nama		: "Paket Asik",
		deskripsi	: "3-pc Chickenjoy W/ 1 Side",
		harga		: "55.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc2side.png",
		nama		: "Paket Seru",
		deskripsi	: "3-pc Chickenjoy W/ 2 Side",
		harga		: "57.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketa.png",
		nama		: "Paket Enak",
		deskripsi	: "6-pc Chickenjoy Bucket,  6-pc Burger Steak Family Pack, 3 Peach Mango Pies",
		harga		: "66.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketb.png",
		nama		: "Paket Uenak",
		deskripsi	: "6-pc Chickenjoy Bucket, 1 Spaghetti Family Pack, 3 Peach Mango Pies",
		harga		: "66.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketc.png",
		nama		: "Paket Sedap",
		deskripsi	: "6-pc Chickenjoy Bucket, 1 Palabok Family Pack, 3 Peach Mango Pies",
		harga		: "66.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketd.png",
		nama		: "Paket Sedap Juga",
		deskripsi	: "10-pc Chickenjoy Bucket, 5 Steamed Rice,  5 Peach Mango Pies",
		harga		: "66.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc1side.png",
		nama		: "Paket Nagih",
		deskripsi	: "3-pc Chickenjoy W/ 1 Side",
		harga		: "55.000"
	},	{
		foto		: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc1side.png",
		nama		: "Paket Rindu",
		deskripsi	: "3-pc Chickenjoy W/ 1 Side",
		harga		: "55.000"
	}
];

//const body = document.querySelector(".food");
//
//for(let data of daftarMakanan){
//	body.innerHTML += `
//			 <div class="row">
 //						 <div class="col-sm-6">
 	//					 	<div class="card" style="width: 18rem; display: flex;">
  	//							<img class="card-img-top" src="${data.foto}">
  	//							<div class="card-body">
    //								<h5 class="card-title">${data.deskripsi}</h5>
    //								<p class="card-text">Harga ${data.harga}</p>
    //								<a href="#" class="btn btn-primary" style="background:#cd1338; border: none;">Pesan</a>
  	//							</div>
	//						</div>
	//					</div>
	//					
	//				</div>`;
	//				
//}

const callBackMap = (data, index) => {
    let cardMakanan = `
			<div class="col mb-4">
 						 	<div class="card  card-makanan" style="margin: 0% 29.5%; margin-bottom: 16%; border-radius: 10px;">
  								<img src="${data.foto}" class="card-img-top" 
  								
  								<div class="card-body">
    								<h4 class="card-title" style="color:#cd1338">${data.nama}</h4>
    								<p class="card-text">Harga ${data.deskripsi}</p>
    								<h5 class="card-text">Harga ${data.harga}</h5>
    								<a href="#" class="btn btn-primary add-cart" style="background:#cd1338; border: none;">Pesan</a>
  								</div>
							</div>
						
			</div>`;

const daftarMakanan = document.querySelector(".food");
    daftarMakanan.innerHTML += cardMakanan;
  }

daftarMakanan
  .sort((a, b) => (a.harga > b.harga ? 1 : b.harga > a.harga ? -1 : 0))
  .map(callBackMap);

const buttonElmnt = document.querySelector('.search-button');

buttonElmnt.addEventListener('click', () =>{
	const searchBox = daftarMakanan.filter((data,index) =>{
 			const inputElmnt  = document.querySelector('.input-keywords');
 			const itemName	  = data.nama.toLowerCase();
 			const keyword	  = inputElmnt.value.toLowerCase();

 			return itemName.includes(keyword);
 			})

	document.querySelector('.food').innerHTML = '';
	
});


 