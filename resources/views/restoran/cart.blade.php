<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Jollibie</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/cart.css') }}">
</head>
<body>
	<body>
	<div class="wrapper">
		<div class="sidebar">
			<img src="images/jollibee-logo-1.png">
			<ul>
				<li><a href="">Tentang Kami</a></li>
				<li><a href="">Makanan Kami</a></li>
				<li><a href="">Buzz Room</a></li>
				<li><a href="">Perkerjaan/Karir</a></li>
				<li><a href="">Hubungi Kami</a></li>
			</ul>
			<div class="social-media">
			<li><a href="https://www.instagram.com/jollibeeus/?hl=en"><i class="fab fa-instagram"></i></a></li>
			<li><a href="https://web.facebook.com/JollibeeUS/?_rdc=1&_rdr"><i class="fab fa-facebook-f"></i></a></li>
		</div>
	</div>
		<div class="main">
			<table>
				<tr>
					<th>Judul</th>
					<th>Desc</th>
					<th>Harga</th>
				</tr>
				@foreach($daftarmenu as $menu)
				<tr>
					<td>{{$menu->judul}}</td>
					<td>{{$menu->desc}}</td>
					<td>{{$menu->harga}}</td>
				</tr>
				@endforeach
			</table>

			
			
			<div class="total-harga">
				
			</div>
		</div>
	

	<div class="footer">
			<div class="footer-content">
				<div class="footer-section kiri">
					<a href="">Nilai Perusahaan</a>
					<a href="">Aksesibilitas</a>
					<p>&copy Muhammad Ihsan H All Rights Reserved</p>
				</div>
				<div class="footer-section-kanan">
					<a href="">Kebijakan Privasi</a>
					<a href="">Syarat dan Ketentuan</a>
				</div>
			</div>
			<div class="copyright">
				&copy Muhammad Ihsan H All Rights Reserved
			</div>

	</div>
	

<script src="shopping-cart.js"></script>
</body>
</html>

