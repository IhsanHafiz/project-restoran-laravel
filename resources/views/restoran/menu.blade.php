<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Jollibie</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/menu.css') }}">
</head>
<body>
	<body>
	<div class="wrapper">
		<div class="sidebar">
			<img src="images/jollibee-logo-1.png">
			<ul>
				<li><a href="">Tentang Kami</a></li>
				<li><a href="">Makanan Kami</a></li>
				<nav class="navbar navbar-light">
 					<div class="form-inline">
   					<input class="form-control mr-sm-2 	input-keywords" type="search" placeholder="Search" aria-label="Search" id="searchBox" name="searchBox" style="width: 100%;">
    				<button class="search-button" type="submit">Search</button>
  					</div>
				</nav>
				<li><a href="">Buzz Room</a></li>
				<li><a href="">Perkerjaan/Karir</a></li>
				<li><a href="">Hubungi Kami</a></li>
				<li><button style="font-size:24px; width: 100%; border: none; color: #cd1338; margin-bottom: 10px;" onclick="window.location.href='cart';"><i class="fa fa-shopping-cart"></i></button></li>
			</ul>
			<div class="social-media">
			<li><a href="https://www.instagram.com/jollibeeus/?hl=en"><i class="fab fa-instagram"></i></a></li>
			<li><a href="https://web.facebook.com/JollibeeUS/?_rdc=1&_rdr"><i class="fab fa-facebook-f"></i></a></li>
		</div>
	</div>
	<div class="main_content">
			<div class="food">
				<h1>Menu Kami</h1>
			</div>
			<div></div>

	</div>

	<div class="footer">
			<div class="footer-content">
				<div class="footer-section kiri">
					<a href="">Nilai Perusahaan</a>
					<a href="">Aksesibilitas</a>
					<p>&copy Muhammad Ihsan H All Rights Reserved</p>
				</div>
				<div class="footer-section-kanan">
					<a href="">Kebijakan Privasi</a>
					<a href="">Syarat dan Ketentuan</a>
				</div>
			</div>
			<div class="copyright">
				&copy Muhammad Ihsan H All Rights Reserved
			</div>

	</div>
	

<script type="text/javascript" src="{{ URL::asset('js/manipulasi.js') }}"></script>
</body>
</html>