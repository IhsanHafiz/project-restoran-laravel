<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
	<title>Jollibie</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}">
</head>
<body>
	<div class="wrapper">
		<div class="sidebar">
			<img src="{{ asset('images/jollibee-logo-1.png') }}">
			<ul>
				<li><a href="">Tentang Kami</a></li>
				<li><a href="menu">Makanan Kami</a></li>
				<li><a href="">Buzz Room</a></li>
				<li><a href="">Perkerjaan/Karir</a></li>
				<li><a href="">Hubungi Kami</a></li>

			</ul>
			<div class="social-media">
			<li><a href="https://www.instagram.com/jollibeeus/?hl=en"><i class="fab fa-instagram"></i></a></li>
			<li><a href="https://web.facebook.com/JollibeeUS/?_rdc=1&_rdr"><i class="fab fa-facebook-f"></i></a></li>
		</div>
		</div>
		
		<div class="main_content">
			<div class="header">
								Kami peduli dengan keselamatan Anda. <a href="https://jollibeeusa.com/safety/">Klik</a> tautan ini untuk membaca tentang langkah-langkah yang kami ambil untuk memastikan kami melindungi kesehatan dan kesejahteraan para tamu, anggota tim, dan komunitas tempat kami melayani di tengah wabah COVID-19.
			</div>
			<div class="info">
				<div class="slidershow middle">
					
					<div class="slides">
						<input type="radio" name="r" id="r1" checked>
						<input type="radio" name="r" id="r2">
						<input type="radio" name="r" id="r3">
						<input type="radio" name="r" id="r4">
						<input type="radio" name="r" id="r5">
					<div class="slide s1">
						<a href="https://jollibeeusa.com/locations/"><img src="images/banner1.jpg"></a>
					</div>
					<div class="slide">
						<a href="https://www.doordash.com/business/jollibee-5095/"><img src="images/banner2.jpg"></a>
					</div>
					<div class="slide">
						<video autoplay loop>
							<source src="images/banner3.mp4" type="video/mp4">
						</video>
					</div>
					<div class="slide">
						<video autoplay loop>
							<source src="images/banner4.mp4" type="video/mp4">
						</video>
					</div>
					<div class="slide">
						<video autoplay loop>
							<source src="images/banner5.mp4" type="video/mp4">
						</video>
					</div>
					</div>

					<div class="navigation">
						<label for="r1" class="bar" onclick="currentSlide(1)"></label>
						<label for="r2" class="bar" onclick="currentSlide(2)"></label>
						<label for="r3" class="bar" onclick="currentSlide(3)"></label>
						<label for="r4" class="bar" onclick="currentSlide(4)"></label>
						<label for="r5" class="bar" onclick="currentSlide(5)"></label>
					</div>
				</div>

			</div>
			</div>
		</div>

		<div>	
				<div class="food">
					<h1>Makanan Khas Kami</h1>
					<img src="images/signaturechickjoy.png">
					<h3>Chicken Joy</h3>
					<br>
					<p>Ayam goreng khas kami, yang dibumbui dengan sempurna agar renyah di luar dan juicy di dalam.</p>
					<button onclick="location.href='menu'" type="button">
						Lihat Menu Kami
					</button>

				</div>
		</div>
		
		</div>

	</div>

	<div class="footer">
			<div class="footer-content">
				<div class="footer-section kiri">
					<a href="">Nilai Perusahaan</a>
					<a href="">Aksesibilitas</a>
					<p>&copy Muhammad Ihsan H All Rights Reserved</p>
				</div>
				<div class="footer-section-kanan">
					<a href="">Kebijakan Privasi</a>
					<a href="">Syarat dan Ketentuan</a>
				</div>
			</div>
			<div class="copyright">
				&copy Muhammad Ihsan H All Rights Reserved
			</div>

	</div>

<script>
	
	var slideIndex = 1;
showSlides(slideIndex);


function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slide");
  var dots = document.getElementsByClassName("bar");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}

</script>

</body>
</html>